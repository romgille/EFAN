# EFAN
Introduction à la recherche sur le thème de la complétion d'image

Le rapport comportera 3 parties :


# Synthèse de l’article
Cette partie est un résumé critique de l’article (longueur : environ 6000 caractères).
Vous donnerez en particulier les éléments suivants :

* le contexte : quelle est la problématique générale abordée ?
* les objectifs : quel est le but de la méthode proposée dans l’article ?
* les hypothèses : pourquoi l’approche proposée est-elle pertinente pour atteindre les objectifs ?
* la méthode : comment fonctionne la méthode proposée ?
* méthodologie de validation : quels sont les tests proposés par les auteurs pour valider leurs
    hypothèses ? pour valider la méthode ?
* les résultats : quels sont les résultats des tests ?
* opinion personnelle : quel est votre impression personnel sur cet article ?
    (intérêt, qualité d’écriture, des hypothèses, de la méthode, des tests…)


# Etude de l’implémentation
Cette partie a pour but d’étudier l’implémentation fournie par les auteurs ou un tiers
(longueur : environ 3000 caractères). Vous donnerez en particulier les éléments suivants :

* la portée des programmes fournis : le code fourni permet-il de reproduire la totalité des
    expériences présentées dans l’article ?
* la structure : quelles sont les interfaces fournies ? le code est-il bien structuré ?
    pensez-vous pouvoir le réutiliser dans une autre contexte ?
* la fidélité : le code fourni correspond-il à ce qui est décrit dans l’article ?
    Si non, quelles sont les différences notables ?


# Expérimentations
Cette partie a pour but de vérifier que tout où partie des expériences réalisées dans l’article
sont reproductibles (longueur : environ 3000 caractères). Vous donnerez en particulier les éléments
suivants :

* expériences reproduites : décrivez les expériences que vous avez choisies de reproduire.
* configuration de tests : précisez les conditions d’expérimentation (choix des images,
    des paramètres).
* analyse des résultats : analysez les résultats obtenus, sont-ils conformes aux résultats
    présentés dans l’article ?
* expériences complémentaires : pouvez-vous proposer des expériences complémentaires pour évaluer
    la méthode ?

